/**
 * Das QueueInterface wird von Queue implementiert. Dabei werden die Methoden addElement, removeElement und isEmpty implementiert. 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 */



public class Queue implements QueueInterface {

	// F�r den Arbeitsprozess benutzten wir diese einfachen Tests, um ein vorl�ufig korrektes Ablaufen unserer Methoden zu �berpr�fen.
	public static void main(String[] args){
		//Queue testQueue = new Queue(5);
		//testQueue.addElement(1);
		//testQueue.removeElement();
		//testQueue.addElement(9);
		//testQueue.output();   	
	 }
	
    // Erstes Array mit int-Elementen
    private int[] newQueue;
    
    //Mit dem Datentyp boolean pr�fen wir, ob das Array leer ist.
    private boolean empty;
    
    //Anzahl der in der Array enthaltenen Elemente. Somit repr�sentiert number die erste leere Stelle in der Array, wo ein neues Element eingef�gt werden kann.
    private int number;

    //Konstruktor erstellen
    public Queue(int queueSize){
    	newQueue = new int[queueSize];
    	empty = true;
    	number = 0;
    }
    
    /*
     * Diese Methode f�gt ein Element der Array hinzu. 
     * Wenn das Array aber voll ist, dann muss ein doppelt so grosses Array erstellt werden.
     */
    @Override
    public void addElement(int element){

        if (isEmpty()){
            newQueue[0] = element;
            number++;		//Da ein Element hinzugef�gt wurde, wird number um eins erh�ht.
            empty = false;		//Deswegen ist das Array nicht mehr leer --> somit false.
        }
        
        /*
         * Wenn die volle Anzahl erreicht ist und eine neues Element hinzugef�gt wird, gibt es laut number keinen freien Platz mehr.
         * Somit wir ein doppelt so langes Array, wie das urspr�ngliche, erstellt.
         */
        else {
        	if (number == newQueue.length){
        		newQueue = doubleQueueSize(newQueue);
        	}
        	newQueue[number] = element;
        	number++;
        }
    }
    
     /**
      * Hier wird ein doppelt so grosses Array erstellt, welches die Elemente des vorherigen, bereits vollen Arrays �bernimmt.
      * @return doubleQueue wird als doppelt so grosses Array zur�ckgegeben (inkl. der alten Elemente).
      */
     private int [] doubleQueueSize (int[] oldQueue){
    	 int newQueueSize = oldQueue.length * 2;
    	 int [] doubleQueue = new int [newQueueSize];
    	 for (int i = 0; i < oldQueue.length; i++){
    		 int oldNumber = oldQueue[i];
    		 if (oldNumber != 0){
    			 doubleQueue[i] = oldQueue[i];
    			 
    		 }
    	 }
    	 return doubleQueue;
     }

    /**
     * Diese Methode pr�ft ob das Array leer ist. Wenn ja, wird empty auf true gesetzt.
     * Wenn nein, wird nach dem FIFO-Prinzip das erste Element gel�scht.
     * @return newQueueSize, gibt die neue Queue nach dem L�schen des jeweils ersten Elementes zur�ck.
     */
    @Override
    public void removeElement() {

        if (!isEmpty()){
            newQueue = removeFirstElement(newQueue);
            number--;
        }
        if (number == 0){
        	empty = true;
        }
       }
    
    private int [] removeFirstElement(int[] oldQueue){
    	int[] newQueueSize = new int[oldQueue.length];
    	for(int i = 0; i < oldQueue.length -1; i++){
    		newQueueSize[i] = oldQueue[i + 1]; 		// Index wird auf 1 gesetzt, weswegen die neue Queue erst ab der zweiten Position ausgegeben wird.
    	}
    	return newQueueSize;
    }
        
     
    /**
     * @return true, wenn das Array leer ist.
     */
    @Override
    public boolean isEmpty() {
        return empty;
    }

    //Hilfsfunktion
    private void output(){
    	for(int element : newQueue){
    		System.out.println(element);
    	}
    }
    
    
}